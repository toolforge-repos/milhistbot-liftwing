﻿using System;
using Wikimedia;

public class Liftwing
{
    private static void usage()
    {
        Console.Error.WriteLine ("usage: Liftwing <article>");
        System.Environment.Exit(1);
    }

    private Liftwing(string name)
    {
        try
        {
            Bot bot = new Bot();
            Page page = new Page(bot, name);
            Console.WriteLine(page.Prediction());
        }
        catch (BotException)
        {
            Environment.Exit(1);
        }
    }

    static public void Main(string[] args)
    {
        if (!args.Any())
        {
            usage();
        }
        Liftwing test = new Liftwing(args[0]);
    }

}

