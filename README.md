# NAME

Liftwing - Evaluate a Wikipedia article using Liftwing

# USAGE

Liftwing <article>

# DESCRIPTION

Test program for access to Liftwing and Dotnet. 

This requires a configuration file (not supplied) and the latest version of Dotnet.  

